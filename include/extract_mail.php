<html>

<head>

<meta http-equiv="content-type" content="text/html; charset=utf-8" />

<style>

div.toggler { border:1px solid #ccc; background:url(gmail2.jpg) 10px 12px #eee no-repeat; cursor:pointer; padding:10px 32px; }
div.toggler .subject { font-weight:bold; }
div.read { color:#666; }
div.toggler .from, div.toggler .date { font-style:italic; font-size:11px; }
div.body { padding:10px 20px; }

</style>

</head>

<body>

<?php


require_once "connect_functions.php";


$charset = '';
$htmlmsg = '';
$plainmsg = '';
$attachments = '';


function getmsg ($mbox,$mid) {
    // input $mbox = IMAP stream, $mid = message id
    // output all the following:
    global $charset, $htmlmsg, $plainmsg, $attachments;

    $htmlmsg = $plainmsg = $charset = '';
    $attachments = array();

    // HEADER
    $h = imap_header($mbox,$mid);
    // add code here to get date, from, to, cc, subject...

    // BODY
    $s = imap_fetchstructure($mbox,$mid);
    if (!$s->parts)  // simple
        getpart($mbox,$mid,$s,0);  // pass 0 as part-number
    else {  // multipart: cycle through each part
        foreach ($s->parts as $partno0=>$p)
            getpart($mbox,$mid,$p,$partno0+1);
    }
}

function getpart ($mbox,$mid,$p,$partno) {
    // $partno = '1', '2', '2.1', '2.1.3', etc for multipart, 0 if simple
    global $htmlmsg, $plainmsg, $charset, $attachments, $filename, $attachmentFileArr, $attachmentDataArr;

    // DECODE DATA
    $data = ($partno) ?
        imap_fetchbody($mbox,$mid,$partno,FT_PEEK):  // multipart
        imap_body($mbox,$mid,FT_PEEK);  // simple
    // Any part may be encoded, even plain text messages, so check everything.
    if ($p->encoding==4)
        $data = quoted_printable_decode($data);
    elseif ($p->encoding==3)
        $data = base64_decode($data);

    // PARAMETERS
    // get all parameters, like charset, filenames of attachments, etc.
    $params = array();
    if ($p->parameters)
        foreach ($p->parameters as $x)
	{
		if (strtolower($x->attribute) == 'name' || preg_match ('~name~i', $x->attribute))
			$params['name'] = $x->value;
            //$params[strtolower($x->attribute)] = $x->value;
	}
    if (isset ($p->dparameters))
        foreach ($p->dparameters as $x)
	{
		if (strtolower($x->attribute) == 'filename' || preg_match ('~filename~i', $x->attribute))
			$params['filename'] = $x->value;
            //$params[strtolower($x->attribute)] = $x->value;
	}

    // ATTACHMENT
    // Any part with a filename is an attachment,
    // so an attached text file (type 0) is not mistaken as the message.
    if (isset ($params['filename']) || isset ($params['name'])) {
        // filename may be given as 'Filename' or 'Name' or both
        $filename = isset ($params['filename']) ? $params['filename'] : $params['name'];
		$attachmentFileArr[] = $filename;
		$attachmentDataArr[] = $data;
        // filename may be encoded, so see imap_mime_header_decode()
        $attachments[$filename] = $data;  // this is a problem if two files have same name
    }

    // TEXT
    if ($p->type==0 && $data) {
        // Messages may be split in different parts because of inline attachments,
        // so append parts together with blank row.
        if (strtolower($p->subtype)=='plain')
            $plainmsg .= trim($data) ."\n\n";
        else
            $htmlmsg .= $data ."<br><br>";
        //$charset = $params['charset'];  // assume all parts are same charset
    }

    // EMBEDDED MESSAGE
    // Many bounce notifications embed the original message as type 2,
    // but AOL uses type 1 (multipart), which is not handled here.
    // There are no PHP functions to parse embedded messages,
    // so this just appends the raw source to the main message.
    elseif ($p->type==2 && $data) {
        $plainmsg .= $data."\n\n";
    }

    // SUBPART RECURSION
    if (isset ($p->parts)) {
        foreach ($p->parts as $partno0=>$p2)
            getpart($mbox,$mid,$p2,$partno.'.'.($partno0+1));  // 1.2, 1.2.1, etc.
    }
}

//return supported encodings in lowercase.
function mb_list_lowerencodings () {
	$r = mb_list_encodings ();
	for ($n = sizeOf($r) ; $n-- ;) {
		$r[$n] = strtolower ($r[$n]);
	}
	return $r;
}

// Receive a string with a mail header and returns it
// decoded to a specified charset.
// If the charset specified into a piece of text from header
// isn't supported by "mb", the "fallbackCharset" will be
// used to try to decode it.
// change targetCharset to 'utf-8' if meta-tag charset is utf-8
function decodeMimeString($mimeStr, $inputCharset='utf-8', $targetCharset='utf-8', $fallbackCharset='iso-8859-7') {
  $encodings=mb_list_lowerencodings();
  $inputCharset=strtolower ($inputCharset);
  $targetCharset=strtolower ($targetCharset);
  $fallbackCharset=strtolower ($fallbackCharset);

  $decodedStr='';
  $mimeStrs=imap_mime_header_decode($mimeStr);
  for ($n=sizeOf($mimeStrs), $i=0; $i<$n; $i++) {
    $mimeStr=$mimeStrs[$i];
    $mimeStr->charset=strtolower($mimeStr->charset);
    if (($mimeStr == 'default' && $inputCharset == $targetCharset)
      || $mimeStr->charset == $targetCharset) {
      $decodedStr.=$mimeStr->text;
    } else {
      $decodedStr.=mb_convert_encoding ($mimeStr->text, $targetCharset, (in_array($mimeStr->charset, $encodings) ? $mimeStr->charset : $fallbackCharset));
    }
  } return $decodedStr;
}

set_time_limit (0);

//error_reporting (0);


if (!function_exists ("imap_open"))
{
	//echo "<br>Please enable IMAP library";
	echo "-2";
	die (0);
}


$host = MAIL_HOST;
$login = MAIL_LOGIN;
$pass = MAIL_PASSWORD;


$resrc = imap_open ($host, $login, $pass);

if ($resrc === false)
{
	// problem connecting to mail server
	echo "-1";
	die (0);
}


/* grab emails */

$method = MAIL_TRANSFER_METHOD;

if ($method == 2)
{
	$emails = imap_search ($resrc, 'UNSEEN');
	$emails_uid = array (0, 1);
}
else
{
    	$emails = imap_search ($resrc, 'ALL');
	$emails_uid = imap_search ($resrc, 'ALL', SE_UID);
}


$count_new_emails = 0;

if ($emails) {
        
	connect_db ();
	
	mysql_query ('set character set utf8 ');

	/* put the oldest emails on top so that they are stored to DB in reverse order */
	sort ($emails);
	sort ($emails_uid);


	if ($method == 1)
	{
		$query = "select mail_id from registry limit 1";
		$res = mysql_query ($query);

		$mail_limit = 0;
		if ($res)
		{
			$arr = mysql_fetch_array ($res);
			$mail_limit = $arr["mail_id"];
                        
		}

		if ($mail_limit == 0)
		{
			$mail_limit = end ($emails_uid) - 1;
		}

	}

	
	foreach ($emails as $key => $email_number)
	{
	
		if ($method == 1)
		{
			$current_uid = isset ($emails_uid[$key]) ? $emails_uid[$key] : $mail_limit;
			if ($current_uid <= $mail_limit)
			{
                           continue;
			}
		}

		$count_new_emails++;
                
		/* get information specific to this email */
		$overview = imap_fetch_overview ($resrc, $email_number, 0);
		$structure = imap_fetchstructure ($resrc, $email_number);

		if ($method == 1)
		{
			$message = imap_fetchbody ($resrc, $email_number, 2, FT_PEEK);
		}
		else // mark the message as "read"
		{
			$message = imap_fetchbody ($resrc, $email_number, 2);
		}

		if (isset($structure->parts) && is_array ($structure->parts) && isset ($structure->parts[1])) {
		    $part = $structure->parts[1];
		    $message = imap_fetchbody ($resrc, $email_number, 2, FT_PEEK);

		    if ($part->encoding == 3) {
		        $message = imap_base64 ($message);
		    } else if ($part->encoding == 1) {
		        $message = imap_8bit ($message);
		    } else {
		        $message = imap_qprint ($message);
		    }
		}

		$charset = '';
		$htmlmsg = '';
		$plainmsg = '';
		$attachments = '';
		$filename = '';
		$attachmentFileArr = Array ();
		$attachmentDataArr = Array ();
		getmsg ($resrc, $email_number);

		$body = @decodeMimeString ($plainmsg);
		$body2 = @decodeMimeString ($plainmsg, "utf-8", "utf-8", "utf-8");

		if (urlencode ($body2) === urlencode ($plainmsg)) $body = $body2;

		//$message = strip_tags ($message);
		$uid = imap_uid ($resrc, $email_number);

		$subject = $overview[0]->subject;
		$from = $overview[0]->from;
		$header = imap_header ($resrc, $email_number); // get first mails header		
		$from_address = $header->from[0]->personal;
		$sender_email_address = $header->from[0]->mailbox . "@" . $header->from[0]->host;

		$subject = decodeMimeString ($subject);
		$message = decodeMimeString ($message);
		$from = decodeMimeString ($from);

		// DATE FORMAT:
		// yyyy-mm-dd hh:mm:ss		
		$date = $overview[0]->date;
		$date = strtotime ($date); // convert to timestamp
		//$date = $date + 60*60; // to fix the bug of 1 hour in email time
		$date = date ("Y-m-d H:i:s", $date);
		

		$mail_address = $sender_email_address;
		$rt = $date;
		$entry_id = $uid;
		$from = mysql_real_escape_string ($from);
		$subject = mysql_real_escape_string ($subject);
		$body = mysql_real_escape_string ($body);
		$mail_address = mysql_real_escape_string ($mail_address);
		$query = "insert into mails (mails.from, subject, body, entry_id, rt, mail_address) values ('$from', '$subject', '$body', '$entry_id', '$rt', '$mail_address');";
				
		$res = 0;
		$res = mysql_query ($query);
		$last_insert_id = mysql_insert_id ();
		
		// attachment handling
		$attachments = array ();
		if (isset ($structure->parts) && count ($structure->parts)) {

			for ($i = 0; $i < count ($structure->parts); $i++) {

			    $attachments[$i] = array(
				'is_attachment' => false,
				'filename' => '',
				'name' => '',
				'attachment' => ''
			    );

			    if ($structure->parts[$i]->ifdparameters) {
				foreach($structure->parts[$i]->dparameters as $object) {
				    if(strtolower($object->attribute) == 'filename' || preg_match ('~filename~i', $object->attribute)) {
					$attachments[$i]['is_attachment'] = true;
					$attachments[$i]['filename'] = $object->value;
				    }
				}
			    }

			    if ($structure->parts[$i]->ifparameters) {
				foreach($structure->parts[$i]->parameters as $object) {
				    if(strtolower($object->attribute) == 'name' || preg_match ('~name~i', $object->attribute)) {
					$attachments[$i]['is_attachment'] = true;
					$attachments[$i]['filename'] = $object->value;
				    }
				}
			    }

			    if ($attachments[$i]['is_attachment']) {
				$attachments[$i]['attachment'] = imap_fetchbody($resrc, $email_number, $i+1, FT_PEEK);
				if($structure->parts[$i]->encoding == 3) { // 3 = BASE64
				    $attachments[$i]['attachment'] = base64_decode($attachments[$i]['attachment']);
				}
				else if ($structure->parts[$i]->encoding == 4) { // 4 = QUOTED-PRINTABLE
				    $attachments[$i]['attachment'] = quoted_printable_decode($attachments[$i]['attachment']);
				}
			    }
			}
		}
	

		//$folder = "/var/www/eprotocol_dev/attachments/files/";
		$folder = ATTACHMENT_FOLDER;
		
		$attNo = 0;
		foreach ($attachmentFileArr as $key => $fileName)
		{
		//foreach ($attachments as $key => $attachment) {
		//	if ($attachment['is_attachment'])
			{
				//$attachmentFileName = isset ($attachmentFileArr[$key]) ? $attachmentFileArr[$key] : $attachment['filename'];
				$attachmentFileName = isset ($attachments[$key]['filename']) ? $attachments[$key]['filename'] : $fileName;
				$attachmentFileName = $attachmentFileArr[$key];
				$contents = isset ($attachmentDataArr[$key]) ? $attachmentDataArr[$key] : "";

				// remove 'utf-8' string appearing in front of some attachment file names
				if (strtolower (substr ($attachmentFileName, 0, 5)) == 'utf-8')
					$attachmentFileName = substr ($attachmentFileName, 5);
					
				$attNo++;

				// to onoma arxeiou pou tha apothikeutei sth BD prepei na einai se utf-8
				$name_db = decodeMimeString ($attachmentFileName, "utf-8", "utf-8");
				
				//$name = decodeMimeString ($attachmentFileName, "utf-8", "utf-8"); // ubuntu
				//$name = decodeMimeString ($attachmentFileName, "iso-8859-7", "iso-8859-7"); // windows
				$name = decodeMimeString ($attachmentFileName, FILENAME_ENCODING, FILENAME_ENCODING);
				

				//$name = imap_mime_header_decode ($name);
				$name = urldecode ($name);
				$name = trim ($name);
				$name = str_replace (',', '_', $name);
				$name = str_replace ('%', '_', $name);	
				$name = str_replace (' ', '_', $name);
				$name = str_replace ('-', '_', $name);
				$name = str_replace ('"', '_', $name);
				$name = str_replace ("'", '_', $name);
				$name = $attNo . "_" . $name;

				//$name_db = imap_mime_header_decode ($name_db);
				$name_db = urldecode ($name_db);
				$name_db = trim ($name_db);
				$name_db = str_replace (',', '_', $name_db);
				$name_db = str_replace ('%', '_', $name_db);				
				$name_db = str_replace (' ', '_', $name_db);
				$name_db = str_replace ('-', '_', $name_db);
				$name_db = str_replace ('"', '_', $name_db);
				$name_db = str_replace ("'", '_', $name_db);
				$name_db = $attNo . "_" . $name_db;
			
				//$contents = $attachment['attachment'];

				//$result = file_put_contents ($folder . $last_insert_id . "_" . $name, $contents);
				$file_to_store = $folder . $last_insert_id . "_" . $name;
				$result = file_put_contents ($file_to_store, $contents);
				
				// apothikeush kathe attachment ston pinaka attachments
			    // strSQL = "INSERT INTO attachements ( `entry_id`,`mail_id`,`file_name`) VALUES ('" & mai.EntryID & "', " & last_insert_id & ", '" & short_file & "')"
				
				$name_db = mysql_real_escape_string ($name_db);
				
				$query = "insert into attachements (mail_id, entry_id, file_name) values ('$last_insert_id', '$entry_id', '$name_db');";
				$res = mysql_query ($query);
				
			}
		}


		$last_mail_id = $uid;
		if ($method == 2) $last_mail_id = 0;
		$sql = "update registry set mail_id=" . $last_mail_id;
		@mysql_query ($sql);

	}

} 

imap_close ($resrc);

?>
