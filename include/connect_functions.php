<?php

require_once "constants.inc";

    function connect_db()
    {
        $conn=0;
        $db=@mysql_connect(DB_HOST,DB_USER,DB_PASSWORD);
        if($db)
        {
            $db_selected=@mysql_select_db(DB_NAME);
            if(!$db_selected)
                echo "<p>can't select database " . DB_NAME . "</p>";
        }
        else
        {
            echo "<p>can't connect on " . DB_HOST . "</p>";
        }
        return;
    }

    function mysql2screen($sep="/", $mydate='')
    {
        $out_date='';
        $tmp_table=array();
        $tmp_table=explode("-", $mydate);
        if(count($tmp_table)==3)
        {
            $out_date=$tmp_table[2] . $sep . $tmp_table[1] . $sep . $tmp_table[0];
        }
        return $out_date;
    }
    
    function screen2mysql($sep="/", $mydate='')
    {
        $out_date='';
        $tmp_table=array();
        $tmp_table=explode($sep, $mydate);
        $len=count($tmp_table);
        if($len==3)
        {
            $day=intval($tmp_table[0]);
            $month=intval($tmp_table[1]);
            $year=intval($tmp_table[2]);
        }
        else if($len==2)
        {
            $day=intval($tmp_table[0]);
            $month=intval($tmp_table[1]);
            $year=date("Y");
        }
        else
            return $out_date;
            
        if(checkdate($month, $day, $year))
            $out_date=$year . "-" . $month . "-" . $day;
            
        return $out_date;
    }
?>
