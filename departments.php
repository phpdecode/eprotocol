<?php
    session_start();
    date_default_timezone_set('Europe/Athens');
    include("./include/general_functions.php");
    require_once("./include/connect_functions.php");

    $status=0;
    $user_level='';
    $cur_pass_word='';
    $new_pass_word='';
    $new_pass_word_rep='';
    $error=0;
    $conn_message='';
    
    if(isset($_GET['st']))
    {
        $status=intval($_GET['st']);
        if($status!=1)
            $status=0;
    }
    if(isset($_SESSION['usrlvl']) && $_SESSION['usrlvl'] > 0) {
    $user_level = $_SESSION['usrlvl'];
    $dep_id=$_SESSION['dep_id'];
    header_function(1);
    menu_hor_function($user_level);
    //left_side_function(1);
	

    connect_db();
	@mysql_query ('set character set utf8 ');

	
    if(isset($_POST['sbmtlog']) || isset($_POST['edit_submit_button']))
    {
	
		if (isset ($_POST['dep_edit_id'])) // edit department function
		{
			$dep_id = isset ($_POST["dep_edit_id"]) ? $_POST["dep_edit_id"] : -1;
			$dep_name = isset ($_POST["dep_edit_name"]) ? trim (stripslashes ($_POST["dep_edit_name"])) : "";
			$dep_desc = isset ($_POST["dep_edit_desc"]) ? trim (stripslashes ($_POST["dep_edit_desc"])) : "";
			$dep_username = isset ($_POST["dep_edit_username"]) ? trim (stripslashes ($_POST["dep_edit_username"])) : "";
			$dep_password = isset ($_POST["dep_edit_password"]) ? trim (stripslashes ($_POST["dep_edit_password"])) : "";
			$dep_level = isset ($_POST["dep_level"]) ? $_POST["dep_level"] : 1;		
			$dep_hidden = isset ($_POST["dep_edit_hidden"]) ? $_POST["dep_edit_hidden"] : 0;
			$dep_original_username = isset ($_POST["dep_edit_original_username"]) ? trim (stripslashes ($_POST["dep_edit_original_username"])) : "";
		}
		else // instert new department function
		{
			$dep_id = -1;
			$dep_name = isset ($_POST["dep_name"]) ? trim (stripslashes ($_POST["dep_name"])) : "";
			$dep_desc = isset ($_POST["dep_desc"]) ? trim (stripslashes ($_POST["dep_desc"])) : "";
			$dep_username = isset ($_POST["dep_username"]) ? trim (stripslashes ($_POST["dep_username"])) : "";
			$dep_password = isset ($_POST["dep_password"]) ? trim (stripslashes ($_POST["dep_password"])) : "";
			$dep_level = isset ($_POST["dep_level"]) ? $_POST["dep_level"] : 1;		
			$dep_hidden = isset ($_POST["dep_hidden"]) ? $_POST["dep_hidden"] : 0;
			$dep_original_username = $dep_username;
		}
		
		if ($dep_name === "") $conn_message .= "Το όνομα τμήματος δεν μπορεί να είναι κενό<br />";
		if ($dep_username === "") $conn_message .= "Το όνομα χρήστη δεν μπορεί να είναι κενό<br />";
		if ($dep_password === "") $conn_message .= "Ο κωδικός πρόσβασης δεν μπορεί να είναι κενός<br />";
		else
		if (strlen($dep_password) < 6) $conn_message .= "Ο κωδικός πρόσβασης πρέπει να αποτελείται από τουλάχιστον 6 χαρακτήρες";
		
		$dep_username = mysql_real_escape_string ($dep_username);
		
		$query = "select 1 from departments where user_name='$dep_username'";
		$res = mysql_query ($query);
		$num_res = mysql_num_rows ($res);
		
		if (($num_res > 0 && $dep_id == -1) || ($num_res > 0 && $dep_id > 0 && $dep_original_username !== $dep_username))
		{
			$conn_message .= "Το όνομα χρήστη ($dep_username) υπάρχει ήδη";
		}
			
		$error = 0;
		if ($conn_message !== "")
		{
			$error = 1;
		}

        if($error == 0)
        {
			$dep_name = mysql_real_escape_string ($dep_name);
			$dep_desc = mysql_real_escape_string ($dep_desc);
			$dep_username = mysql_real_escape_string ($dep_username);
			$dep_password = mysql_real_escape_string ($dep_password);

			if ($dep_level != 1 && $dep_level != 2) $dep_level = 1;
			if ($dep_hidden != 0 && $dep_hidden != 1) $dep_hidden = 0;
			
			
			if ($dep_id > 0) // update existing department
			{
				$query = "update departments set name='" . $dep_name . "', des='" . $dep_desc . "', user_name='" . $dep_username . "', pass_word='" . $dep_password . "', hidden=$dep_hidden where id=" . $dep_id;
			}
			else // insert new department
			{
				$query = "insert into departments set name='" . $dep_name . "', des='" . $dep_desc . "', user_name='" . $dep_username . "', pass_word='" . $dep_password . "', level=$dep_level, hidden=$dep_hidden";
			}
			
            $res = mysql_query($query);
			//echo "<br />$query => $res";
            
			//$num_res = mysql_num_rows($res);
			//$num_res = 0;
			
			if (!$res)
            {
				$error++;
            }
        }
		
		echo "<h4>" . $conn_message . "</h4>";
		if ($error > 0)
		{
			echo "<h2 style=\"color: red;\">Η διαδικασία απέτυχε</h2>";
		}
		else
		{
			echo "<h2 style=\"color: blue;\">Η διαδικασία ολοκληρώθηκε επιτυχώς</h2>";
		}
		
    }

	function esc_quotes ($s)
	{
		$s = str_replace ("'", "\\'", $s);
		$s = str_replace ('"', "\\\"", $s);
		$s = str_replace ("\"", "'", $s);
		return $s;
	}

	
    $info_msg="Τμήματα";

    echo "<div id=\"centandrightcontainer\">";  //1

    echo "<div class=\"centcolumn\" id=\"list_id\">";  //2

    echo "<form name=\"frmlogin\" id=\"frmlogin_id\" method=\"post\" action=\"\">";
    echo "<fieldset id=\"fieldset_id\" style>";
	
    echo "<div class=\"msgbar\">" . $info_msg . "</div>";
					
	echo "<table id=\"tbl_list_id\">";
	
	// kefalides pinaka
	echo "<tr>";

	echo "<th>ΑΜ</th>";
	echo "<th class=\"w200\">Ονομα Τμήματος</th>";
	echo "<th>Περιγραφή Τμήματος</th>";
	echo "<th>Ονομα Χρήστη</th>";
	echo "<th>Κωδικός</th>";
	echo "<th>Επίπεδο Χρήστη</th>";
	echo "<th>Απόκρυψη</th>";
	echo "<th>Τελευταία Είσοδος</th>";
	
	echo "</tr>";
	
	$query = "select * from departments";
	
	$res = @mysql_query ($query);
	if ($res)
	{
		$no = 0;
		while ($row = mysql_fetch_array ($res))
		{
			$no++;
			
			$id = $row["id"];
			

			$password = $row["pass_word"];
			//$password = "******"; // TODO : hide or show password ???
			
			$level = $row["level"] == 2 ? "Διαχειριστής" : "Απλός Χρήστης";
			$hidden = (isset ($row["hidden"]) && $row["hidden"] == 1) ? "Ναι" : "Οχι";
			
			$args = "";
			$args .= $id . ",";
			$args .= "'" . esc_quotes ($row["name"]) . "',";
			$args .= "'" . esc_quotes ($row["des"]) . "',";
			$args .= "'" . esc_quotes ($row["user_name"]) . "',";
			$args .= "'" . esc_quotes ($row["pass_word"]) . "',";
			$args .= "'" . $row["level"] . "',";
			$args .= "'" . $row["hidden"] . "'";
			
			$last_login = isset ($row["last_login"]) ? $row["last_login"] : "-";
			if ($last_login == "1980-01-01 00:00:00" || $last_login == "1980/01/01 00:00:00") $last_login_date = "-";
			else
			{
				$last_login_date = date ("d/m/Y (H:i:s)", strtotime ($last_login));
			}
			
			$last_access = isset ($row["last_access"]) ? $row["last_access"] : "-";
			$online = false;
			$last_access_date = "OFFLINE";			
			if ($last_access != "-" && $last_access != "1980-01-01 00:00:00")
			{
				$last_access_date = "ONLINE " . date ("(H:i:s)", strtotime ($last_access));

				// check if there is any access during the last 5 minutes (300 seconds)
				$online = strtotime ($last_access) + 300 >= strtotime ("now");
			}
			
			$online_img = "./images/";
			$online_img .= $online ? "online_dot.png" : "offline_dot.png";
			
			echo "<tr class=\"dpt_highlight\" title=\"Κάντε κλικ για τροποποίηση\" alt=\"Κάντε κλικ για τροποποίηση\" onclick=\"edit_department($args);\">";

			echo "<td>" . $no . "</td>";
			echo "<td class=\"w200\">" . "<img src=\"$online_img\" title=\"$last_access_date\"/>&nbsp;" . $row["name"] . "</td>";
			echo "<td class=\"w200\">" . $row["des"] . "</td>";
			echo "<td class=\"w110\">" . $row["user_name"] . "</td>";
			echo "<td class=\"w80\">" . wordwrap($password,8,"<br>\n",TRUE) . "</td>";
			echo "<td class=\"w110\">" . $level . "</td>";
			echo "<td class=\"w210\">" . $hidden . "</td>";
			echo "<td class=\"w210\" align=\"center\">" . $last_login_date . "</td>";
			
			echo "</tr>";
		}
	}	
	
	echo "</table>";

	echo "<br /><hr />";
	echo "<h3>Προσθήκη νέου τμήματος</h3>";
	
	echo "<table border=\"0\">";

	echo "<tr>";
	echo "<td>Ονομα Τμήματος:</td>";
	echo "<td><input type=\"text\" name=\"dep_name\" value=\"\" class=\"w200 ui-corner-all ui-widget\" /></td>";
	echo "<td>Περιγραφή:</td>";
	echo "<td><input type=\"text\" name=\"dep_desc\" value=\"\" class=\"w200 ui-corner-all ui-widget\" /></td>";
	echo "</tr>";
	
	echo "<tr>";
	echo "<td>Ονομα Χρήστη:</td>";
	echo "<td><input type=\"text\" name=\"dep_username\" value=\"\" class=\"w200 ui-corner-all ui-widget\" /></td>";
	echo "<td>Κωδικός πρόσβασης:</td>";
	echo "<td><input type=\"password\" name=\"dep_password\" value=\"\" class=\"w200 ui-corner-all ui-widget\" /></td>";
	echo "</tr>";	
	
	echo "<tr>";
	echo "<td>Επίπεδο Χρήστη:</td>";
	echo "<td><select name=\"dep_level\" disabled><option value=\"1\">Απλός χρήστης</option><option value=\"2\">Διαχειριστής</option></select></td>";
	echo "<td>Απόκρυψη:</td>";
	echo "<td><select name=\"dep_hidden\"><option value=\"0\">Οχι</option><option value=\"1\">Ναι</option></select></td>";
	echo "</tr>";	

	echo "</table>";
	
	echo "<br />";
	
	echo "<input type=\"submit\" name=\"sbmtlog\" id=\"sbmtlog_id\" class=\" ui-corner-all ui-state-hover w80\"  value=\"Προσθήκη\" />&nbsp;&nbsp;&nbsp;&nbsp;<a href=\"./index.php\">επιστροφή</a>";


	echo "</fieldset>";
    echo "</form>";



     echo "</div>"; //-2


     echo "</div>"; //-1
     //<!--end of center and right column -->
     echo"<div class=\"clear\"></div>";

	
	echo "<div id='modal_dpt_frame' style='display: none; position: fixed; top: 0%; left: 0%; width: 100%; height: 100%; background: gray; color: blue; opacity: 0.5; filter: alpha(opacity=50);'>";
	echo "</div>\n";
	echo "<div id='modal_dpt_message' style='display: none; position: fixed; text-align: center; top: 20%; left: 10%; height: 60%; width: 80%; border: 2px solid green; background: pink; color: blue;'>";
	echo "<h3>Τροποποίηση Τμήματος</h3>";
	echo "<br />";
	
    echo "<form name=\"frmdptedit\" id=\"frmdptedit\" method=\"post\" action=\"\">";	
	
	echo "<table align=\"center\" border=\"1\">";

	echo "<tr>";
	echo "<td>Ονομα Τμήματος:</td>";
	echo "<td><input type=\"text\" id=\"dep_edit_name\" name=\"dep_edit_name\" value=\"\" class=\"w200 ui-corner-all ui-widget\" /></td>";
	echo "<td>Περιγραφή:</td>";
	echo "<td><input type=\"text\" id=\"dep_edit_desc\" name=\"dep_edit_desc\" value=\"\" class=\"w200 ui-corner-all ui-widget\" /></td>";
	echo "</tr>";
	
	echo "<tr>";
	echo "<td>Ονομα Χρήστη:</td>";
	echo "<td><input type=\"text\" id=\"dep_edit_username\" name=\"dep_edit_username\" value=\"\" class=\"w200 ui-corner-all ui-widget\" /></td>";
	echo "<td>Κωδικός πρόσβασης:</td>";
	echo "<td><input type=\"password\" id=\"dep_edit_password\" name=\"dep_edit_password\" value=\"\" class=\"w200 ui-corner-all ui-widget\" /></td>";
	echo "</tr>";	
	
	echo "<tr>";
	echo "<td>Επίπεδο Χρήστη:</td>";
	echo "<td><select id=\"dep_edit_level\" name=\"dep_edit_level\" disabled><option value=\"1\">Απλός χρήστης</option><option value=\"2\">Διαχειριστής</option></select></td>";
	echo "<td>Απόκρυψη:</td>";
	echo "<td><select id=\"dep_edit_hidden\"name=\"dep_edit_hidden\"><option value=\"0\">Οχι</option><option value=\"1\">Ναι</option></select></td>";
	echo "</tr>";

	echo "<tr>";
	
	echo "<td colspan=\"4\">";
	
	echo "<input type=\"submit\" name=\"edit_submit_button\" value=\"Ενημέρωση\" class=\" ui-corner-all ui-state-hover w80\" />";
	echo "&nbsp;&nbsp; &nbsp;&nbsp;";
	echo "<input id='modal_dpt_button' type='button' value='Ακύρωση' class=\" ui-corner-all ui-state-hover w80\" onclick=\"document.getElementById('modal_dpt_frame').style.display='none'; document.getElementById('modal_dpt_message').style.display='none';\" />";
	
	echo "</td>";
	
	echo "</tr>";
	
	echo "</table>";
	

	echo "<input type=\"hidden\" id=\"dep_edit_original_username\" name=\"dep_edit_original_username\" value=\"\"  />";
	echo "<input type=\"hidden\" id=\"dep_edit_id\" name=\"dep_edit_id\" value=\"0\"  />";
	
	echo "<br />";
	
	echo "</form>";	
	
	
	echo "</div>";
	
	 
     footer_small_function();
}
else {
session_destroy();
header("location: " . APP_URL . "login.php");
}
?>

