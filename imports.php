<?php
    date_default_timezone_set('Europe/Athens');
    include("./include/general_functions.php");
    include("./include/connect_functions.php");
    define("MAIL_PP",50);
    header_function(1);
    menu_hor_function();
    left_side_function(1);

    connect_db();
    $num_rows=0;
    $where_clause='';
    $srch_pn='';
    $srch_io_date='';
    $srch_io_auth='';
    if(isset($_POST['srchsbmt']))
    {
        if(isset($_POST['srch_pn']))
        {
            $srch_pn_clause='';
            $srch_pn=trim($_POST['srch_pn']);
            $gr_tbl=explode(">",trim($_POST['srch_pn']));
            if(count($gr_tbl)==2)
            {
                $srch_pn_clause=">" . intval(trim($gr_tbl[1]));
            }
            else
            {
                $gr_tbl=explode("<",$_POST['srch_pn']);
                if(count($gr_tbl)==2)
                {
                    $srch_pn_clause="<" . intval(trim($gr_tbl[1]));
                }
                else
                {
                    $gr_tbl=explode("-",$_POST['srch_pn']);
                    if(count($gr_tbl)==2)
                    {
                        $srch_pn_clause="between " .intval(trim($gr_tbl[0])) . " and " . intval(trim($gr_tbl[1]));
                    }
                    else
                    {
                         $srch_pn_clause="=". intval(trim($_POST['srch_pn']));
                    }

                }


                
            }

            if(trim($srch_pn)!='')
                $where_clause.=" and pn " . $srch_pn_clause;
            else
                $srch_pn='';
        }
        if(isset($_POST['srch_io_date']))
        {
            $srch_io_date=trim($_POST['srch_io_date']);
            $tmp_date=array();
            $tmp_date=explode("/",$srch_io_date);
            if(count($tmp_date)==3)
            {
                if(checkdate($tmp_date[1],$tmp_date[0],$tmp_date[2]))
                {
                    $srch_io_date=$tmp_date[2] . "-" . $tmp_date[1] . "-" . $tmp_date[0];
                    $where_clause.=" and io_date='" . $srch_io_date . "'";
                    $srch_io_date=$_POST['srch_io_date'];
                }
            }
        }
        if(isset($_POST['srch_io_auth']))
        {
            if(strlen($_POST['srch_io_auth'])>0)
            {
                $srch_io_auth=str_replace(" ","%", $_POST['srch_io_auth']);
                $where_clause.=" and io_auth like '%" . $srch_io_auth . "%'";
                $srch_io_auth=$_POST['srch_io_auth'];
            }
        }
        

    }
    $query ="select * from book where status=0 " . $where_clause . " order by pn desc, io_date desc";
    //echo $query;
    @mysql_query ('set character set utf8 ');
    $res=mysql_query ($query);
    $num_rows=mysql_num_rows($res);


     echo "<div id=\"centandrightcontainer\">";  //1

    echo "<div class=\"centcolumn\" id=\"list_id\">";  //2

        echo "<div class=\"pathbar\">";
             if($num_rows > 0)
                echo "<div class=\"impinfo\"><b>" . $num_rows . "</b> εισερχόμενα στις : " . date("d/m/Y H:i:s") . "</div><br />";
            else
                echo "<div class=\"impinfo\"><b>Δεν υπάρχουν</b>εισερχόμενα: " . date("d/m/Y H:i:s") . "</div>";
                
            echo "<form name=\"frmsrchimp\" id=\"frmsrchimp_id\" method=\"post\" action=\"\" >";
            echo "<div class=\"srch\">";
            echo "<label class=\"srch\" for=\"srch_pn\">Αρ.Πρωτ.</label><input type=\"text\" name=\"srch_pn\" id=\"srch_pn_id\"  value=\"" . $srch_pn .  "\" class=\"srch ui-corner-all\" /></div>

            <div class=\"srch\"><label class=\"srch\" for=\"srch_io_date\">Ημ/νία</label><input type=\"text\" name=\"srch_io_date\" id=\"srch_io_date_id\"  value=\"" . $srch_io_date . "\" class=\"srch ui-corner-all\" /></div>";
            echo "<div class=\"srch\"><label class=\"srch\" for=\"srch_io_auth\">Αρχή Έκδ</label><input type=\"text\" name=\"srch_io_auth\" id=\"srch_io_auth_id\"  value=\"" . $srch_io_auth . "\" class=\"srch ui-corner-all w120\" />";
            echo "</div><input type=\"submit\" name=\"srchsbmt\" id=\"sbmtsrch_id\" value=\"search\"  class=\"ui-corner-all ui-state-active w80\" /> <a href=\"#\" id=\"newimport\"><img class=\"act2\"  src=\"./images/inbox.png\" alt=\"inbox\" title=\"Εισερχόμενο\"/> Νέο<br />Εισερχόμενο</a>";
            
            echo "</form>";



        echo "</div>"; //pathbar;




        echo "<div class=\"mailcontainer\">";
        echo "<div class=\"centcolumnpad\" >";
            echo "<table>";
                echo "<tr>";
                    echo "<th>Α.Π.</th>";
                    echo "<th>Ημ/νία</th>";
                    echo "<th>Αρ.Εισ.</th>";
                    echo "<th>Τόπος εκδ</th>";
                    echo "<th>Αρχή εκδ<br />ημ/νία</th>";
                    echo "<th>Περίληψη εισερχ.</th>";
                     echo "<th>τμήμα</th>";
                    
                echo "</tr>";
                if($num_rows > 0)
                {
                    for($i=0;$i<$num_rows;$i++)
                    {
                        $row=mysql_fetch_array($res);
                        echo "<tr>";
                            echo "<td class=\"w60\"><a class=\"imports\" href=\"#\" id=\"aimp_" . $row['id'] ."\">" . $row['pn'] . "</a></td>";
                            echo "<td class=\"w80\">" . $row['io_date'] . "</td>";
                            echo "<td class=\"w60\">" . $row['i_num'] . "</td>";
                            echo "<td class=\"w120\">" . $row['i_place'] . "</td>";
                            echo "<td class=\"w200\">" . $row['io_auth'] . "<br />"  . $row['io_auth_date'] . "</td>";
                            echo "<td class=\"w200\">" . $row['summary'] . "</td>";
                            echo "<td class=\"w120\">";
                            $qry="select b.book_id, b.department_id, d.name from bookdep b inner join departments d on b.department_id=d.id where b.book_id=" . $row['id'];
                            @mysql_query ('set character set utf8 ');
                            $resdep=mysql_query ($qry);
                            $num_dep=mysql_num_rows($resdep);
                            $depts=array();
                            if($num_dep > 0)
                            {

                                for($j=0;$j<$num_dep;$j++)
                                {
                                    $rowdep=mysql_fetch_array($resdep);
                                    array_push($depts,$rowdep['name']);
                                }
                                echo implode("...<br />",$depts);

                            }
                            else
                                "-";
                                
                            echo "</td>";
                            //echo"<td>";
                            
                            //echo "</td>";
                        echo "</tr>";
                    }
                }
            echo "</table>";
        echo "</div>";
        
        echo "<div class=\"centerdivider\"></div>";

        echo "</div>";//mailcontainer
         //echo "<p><a href=\"#\" id=\"newimport\">Νέο Εισερχόμενο</a></p>";

     echo "</div>"; //-2

     echo "<div class=\"centcolumn\" id=\"form_id\">";//2
        echo "<form name=\"frmimport\" id=\"frmimport_id\" enctype=\"multipart/form-data\" method=\"post\" action=\"\">";
       echo "<fieldset id=\"fieldset_id\">";
            echo "<input type=\"hidden\" name=\"check_flag\" id=\"chfl_id\" value=\"5\" />";
            echo "<input type=\"hidden\" name=\"book_id\" id=\"book_id_id\" value=\"\" />";
         echo "</fieldset>";
        echo "</form>";

     /********************
     echo "<p>";
     $timestamp = mktime();
     $now_date=getdate($timestamp);
     echo $now_date['mday'] ."/" .  $now_date['mon'] . "/" . $now_date['year']. " " . $now_date['hours'] . ":" . $now_date['minutes'] . ":" . $now_date['seconds'] ;
     echo "</p>";
     *********************/
     
     echo "</div>"; //-2
     

     echo "</div>"; //-1
     //<!--end of center and right column -->
     echo"<div class=\"clear\"></div>";


     footer_small_function();
?>
