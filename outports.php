<?php
    session_start();
    date_default_timezone_set('Europe/Athens');
    include("./include/general_functions.php");
    require_once("./include/connect_functions.php");
    $status=0;
    $user_level='';
    if(isset($_GET['st']))
    {
        $status=intval($_GET['st']);
        if($status!=1)
            $status=0;
    }
    if(isset($_SESSION['usrlvl']) && $_SESSION['usrlvl'] > 0) {
	
    $user_level = $_SESSION['usrlvl'];
    $dep_id = $_SESSION['dep_id'];
	
	
	$export = isset ($_REQUEST['export']) ? trim ($_REQUEST['export']) : 0;
	
	// Leitourgia Exagogis eiserxomenwn/exerxomenwn
	if ($export > 0)
	{
		$TEXT_LIMIT = 100;
		$ENCODING = "iso-8859-7";

		if ($export == 1) // html form
		{
			echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
			echo "<html xmlns=\"http://www.w3.org/1999/xhtml\">";
			echo "<head>";
			echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />";
			echo "<title>e-protocol - Εξαγωγή</title>";
			echo "</head>";
			echo "<body>";
		}
		else if ($export == 2)// csv form
		{
			header ("Content-type: text/html; charset=$ENCODING");
			header ("Content-type: application/vnd.ms-excel");			
			header ("Content-Disposition: attachment; filename=eprotocol_report_" . date("dmY") . ".csv");
		}
	
		else // Ypografes Larisa
               { 
                        header ("Content-type: text/html; charset=$ENCODING");
			header ("Content-type: application/vnd.ms-excel");			
			header ("Content-Disposition: attachment; filename=eprotocol_ypografes_" . date("dmY") . ".csv");
		}
	
		$from_date = isset ($_REQUEST['from']) ? trim ($_REQUEST['from']) : "";
		$to_date = isset ($_REQUEST['to']) ? trim ($_REQUEST['to']) : "";

		$eidos = $status == 0 ? "Εισερχόμενα" : "Εξερχόμενα";
		
		$str_date = "";
		
		if ($from_date == $to_date) $str_date = $from_date;
		else
		$str_date = "από $from_date ως $to_date";
		
		
		if ($export == 1) echo "<h2>" . $eidos . " ($str_date)</h2>";

		
        $from_date = screen2mysql ("/", $from_date);
        $to_date = screen2mysql ("/", $to_date);
		
		connect_db();
	
		if ($user_level == 1)
		{
			$query = "select b.*, d.department_id from book b inner join bookdep d on b.id=d.book_id where status=$status and io_date between '$from_date' and '$to_date' and department_id=$dep_id order by io_year asc, pn asc, io_date asc";
		}
		else // administrator
		{
			$query = "select * from book where status=$status and io_date between '$from_date' and '$to_date' order by io_year asc, pn asc, io_date asc";
		}
		
		@mysql_query ('set character set utf8 ');
		$res = mysql_query ($query);
		$num_rows = mysql_num_rows($res);

		if ($export == 1)
		{
			echo "<table border='1' style='font-size: 11pt; font-family: Arial, Verdana; border-collapse: collapse; page-break-inside: auto;'>";

			echo "<tr style='background: gray; color: white;'>";
		}
		
		if ($status == 1) // kefalides ekserxomenwn
		{
			if ($export == 1)
			{
				echo "<th width='60px'>Α.Π.</th>";
				echo "<th width='80px'>Ημ/νία</th>";
				echo "<th width='200px'>Αρχή<br />που απευθ.</th>";
				echo "<th width='200px'>Περίληψη εισερχ.</th>";
				echo "<th width='80px'>Ημ/νία διεκπ.</th>";
				echo "<th width='60px'>Σχ. αριθ.</th>";
				echo "<th width='80px'>Φ.Αρχ.</th>";
				echo "<th width='120px'>Τμήμα</th>";
			}
			else
			{
				echo iconv ("utf-8", $ENCODING, "\"Α.Π.\";\"Ημ/νια\";\"Αρχή που απευθ.\";\"Περίληψη εισερχ.\";\"Ημ/νία διεκπ.\";\"Σχ. αριθμ\";\"Φ.Αρχ.\";\"Τμήμα\"");
			}
		}
		else // kefalides ekserxomenwn
		{
			if ($export == 1)
			{		
				echo "<th width='60px'>Α.Π.</th>";
				echo "<th width='80px'>Ημ/νία</th>";
				echo "<th width='60px'>Αρ.Εισ.</th>";
				echo "<th width='120px'>Τόπος εκδ.</th>";
				echo "<th width='200px'>Αρχή εκδ.<br />Ημ/νία</th>";
				echo "<th width='200px'>Περίληψη εισερχ.</th>";
				echo "<th width='120px'>Τμήμα</th>";
			}
			else
			{
				echo iconv ("utf-8", $ENCODING, "\"Α.Π.\";\"Ημ/νια\";\"Αρ.Εισ.\";\"Τόπος εκδ.\";\"Αρχή εκδ. - Ημ/νία\";\"Περίληψη εισερχ.\";\"Τμήμα\"");
			}
		}
		
		if ($export == 1)
		{
			echo "</tr>";
		}
								
		if($num_rows > 0)
		{
			for( $i = 0 ; $i < $num_rows ; $i++)
			{
				$row = mysql_fetch_array ($res);
				
				if ($export == 1)
				{
					echo "<tr style='page-break-inside: avoid; page-break-after: avoid; page-break-before: avoid;'>";
				}
				else
				{
					echo "\n";
				}
				
				if ($status == 1) // stiles ekserxomenwn
				{
					if ($export == 1)
					{
						echo "<td width='60px'>" . $row['pn'] . "</td>";
						echo "<td width='80px'>" . mysql2screen ("/", $row['io_date']) . "</td>";
						echo "<td width='200px'>" . $row['io_auth'] ."</td>";
						echo "<td width='200px'>" . $row['summary'] . "</td>";
						echo "<td width='80px'>" . mysql2screen ("/", $row['io_auth_date']) . "</td>";
						echo "<td width='60px'>" . $row['i_num'] . "</td>";
						echo "<td width='80px'>" . $row['io_folder'] . "</td>";
						echo "<td width='120px' style='font-size: 10pt;'>";
					}
					else
					{
						$pn = str_replace ('"', "'", $row['pn']);
						$io_date = str_replace ('"', "'", mysql2screen ("/", $row['io_date']));
						$io_auth= str_replace ('"', "'", $row['io_auth']);
						$summary = str_replace ('"', "'", $row['summary']);
						$io_auth_date = str_replace ('"', "'", mysql2screen ("/", $row['io_auth_date']));
						$i_num = str_replace ('"', "'", $row['i_num']);
						$io_folder = str_replace ('"', "'", $row['io_folder']);
						
						echo iconv ("utf-8", $ENCODING, "\"$pn\";\"$io_date\";\"$io_auth\";\"$summary\";\"$io_auth_date\";\"$i_num\";\"$io_folder\";");
					}
				}
				else // stiles eiserxomenwn
				{
					if ($export == 1)
					{				
						echo "<td width='60px'>" . $row['pn'] . "</td>";
						echo "<td width='80px'>" . mysql2screen ("/", $row['io_date']) . "</td>";
						echo "<td width='60px'>" . $row['i_num'] . "</td>";
						echo "<td width='120px'>" . $row['i_place'] . "</td>";
						echo "<td width='200px'>" . $row['io_auth'] . "<br />"  .  mysql2screen ("/", $row['io_auth_date']) . "</td>";
						echo "<td width='200px'>" . $row['summary'] . "</td>";
						echo "<td width='120px' style='font-size: 10pt;'>";
					}
					else
					{
						$pn = str_replace ('"', "'", $row['pn']);
						$io_date = str_replace ('"', "'", mysql2screen ("/", $row['io_date']));
						$i_num = str_replace ('"', "'", $row['i_num']);
						$i_place = str_replace ('"', "'", $row['i_place']);
						$io_auth = str_replace ('"', "'", $row['io_auth']);
						$io_auth_date = str_replace ('"', "'", mysql2screen ("/", $row['io_auth_date']));
						$summary = str_replace ('"', "'", $row['summary']);

						// TODO : Einai kalytero na afhnoume ena xarakthra enter metaksy ths Arxhs Ekdoshs kai ths Hmeromhnias ?
						//echo iconv ("utf-8", $ENCODING, "\"$pn\";\"$io_date\";\"$i_num\";\"$i_place\";\"$io_auth " . chr(10) . "$io_auth_date\";\"$summary\";");
						
						echo iconv ("utf-8", $ENCODING, "\"$pn\";\"$io_date\";\"$i_num\";\"$i_place\";\"".wordwrap($io_auth, 50, "\n"). " - $io_auth_date\";\" ".wordwrap($summary, 50, "\n")."\";");
					}
				}
				
				$qry = "select b.book_id, b.department_id, d.name from bookdep b inner join departments d on b.department_id=d.id where b.book_id=" . $row['id'];
				
				@mysql_query ('set character set utf8 ');
				
				$resdep = mysql_query ($qry);
				$num_dep = mysql_num_rows($resdep);
				$depts = array();
				
				if ($num_dep > 0)
				{
					for($j = 0 ; $j < $num_dep ; $j++)
					{
						$rowdep = mysql_fetch_array($resdep);
						array_push ($depts, $rowdep['name']);
					}
					//echo implode("...<br />",$depts);
					$depts_text = implode(" , ",$depts);
					if ($depts_text != "")
					{
						if (mb_strlen ($depts_text, "utf-8") > $TEXT_LIMIT) $depts_text = mb_substr ($depts_text, 0, $TEXT_LIMIT, "utf-8") . "...";
						//if (strlen ($depts_text) > $TEXT_LIMIT) $depts_text = substr ($depts_text, 0, $TEXT_LIMIT) . "...";
						//echo $depts_text;
					}

				}
				else $depts_text = "-";
				
				if ($export == 1)
				{
					echo $depts_text;
				}
				else
				{
					$depts_text = str_replace ("\"", "'", $depts_text);
					echo iconv ("utf-8", $ENCODING, "\"$depts_text\"");
				}

				if ($export == 1)
				{
					echo "</td>";
				
					echo "</tr>";
				}
			}
		}
				
		
		if ($export == 1)
		{		
			echo "</table>";
						
			echo "</body>";
			
			echo "</html>";
		}
				
		die(0);
	}	
	
	
    header_function(1);
    menu_hor_function($user_level);
    //left_side_function(1);




     echo "<div id=\"centandrightcontainer\">";  //1

    echo "<div class=\"centcolumn\" id=\"list_id\">";  //2
    


    
    
    include("./include/outports_list.php");




     echo "</div>"; //-2

     echo "<div class=\"centcolumn\" id=\"form_id\">";//2
        echo "<form name=\"frmimport\" id=\"frmimport_id\" enctype=\"multipart/form-data\" method=\"post\" action=\"\">";
       echo "<fieldset id=\"fieldset_id\">";
            echo "<input type=\"hidden\" name=\"check_flag\" id=\"chfl_id\" value=\"5\" />";
            echo "<input type=\"hidden\" name=\"book_id\" id=\"book_id_id\" value=\"\" />";
            echo "<input type=\"hidden\" name=\"status\" id=\"status_id\" value=\"" . $status . "\" />";
            echo "<input type=\"hidden\" name=\"cur_page\" id=\"cur_page_id\" value=\"" . $page . "\" />";
         echo "</fieldset>";
        echo "</form>";
     echo "</div>";


     //echo "</div>"; //-2


     echo "</div>"; //-1
     //<!--end of center and right column -->
     echo"<div class=\"clear\"></div>";


     footer_small_function();
}
else {
session_destroy();
header("location: " . APP_URL . "login.php");
}
?>

