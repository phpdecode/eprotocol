-- MySQL dump 10.13  Distrib 5.5.41, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: outlook_dev
-- ------------------------------------------------------
-- Server version	5.5.41-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `apopros`
--

DROP TABLE IF EXISTS `apopros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `apopros` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `io_auth` varchar(300) DEFAULT NULL,
  `syntomografia` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1683 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `attachements`
--

DROP TABLE IF EXISTS `attachements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attachements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mail_id` int(10) unsigned NOT NULL,
  `entry_id` varchar(200) NOT NULL,
  `file_name` varchar(200) NOT NULL,
  `rem` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_attachements_1` (`mail_id`),
  CONSTRAINT `FK_attachements_1` FOREIGN KEY (`mail_id`) REFERENCES `mails` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10478 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `book`
--

DROP TABLE IF EXISTS `book`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `book` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `pn` int(10) unsigned NOT NULL,
  `io_date` date NOT NULL,
  `io_year` int(10) unsigned DEFAULT NULL,
  `i_num` varchar(50) DEFAULT NULL,
  `i_place` varchar(250) DEFAULT NULL,
  `io_auth` varchar(250) DEFAULT NULL,
  `io_auth_date` date DEFAULT NULL,
  `io_folder` varchar(50) DEFAULT NULL,
  `rem` varchar(250) DEFAULT NULL,
  `status` tinyint(3) unsigned NOT NULL COMMENT '0 in, 1 out',
  `summary` text,
  `mail_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '0 no,  mail id',
  `announce` text,
  `import_id` int(10) unsigned DEFAULT '0',
  `ada` varchar(45) DEFAULT NULL,
  `fakelos` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Index_2` (`pn`,`io_date`,`status`)
) ENGINE=InnoDB AUTO_INCREMENT=75636 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `bookdep`
--

DROP TABLE IF EXISTS `bookdep`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookdep` (
  `book_id` int(10) unsigned NOT NULL,
  `department_id` int(10) unsigned NOT NULL,
  `ch_date` datetime NOT NULL,
  `accept_time` bigint(20) unsigned DEFAULT NULL,
  PRIMARY KEY (`book_id`,`department_id`),
  KEY `FK_bookdep_1` (`book_id`),
  KEY `FK_bookdep_2` (`department_id`),
  CONSTRAINT `FK_bookdep_1` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_bookdep_2` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `departments`
--

DROP TABLE IF EXISTS `departments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `departments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(250) DEFAULT NULL,
  `des` varchar(250) DEFAULT NULL,
  `user_name` varchar(50) NOT NULL,
  `pass_word` varchar(250) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL,
  `level` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `hidden` int(11) NOT NULL DEFAULT '0',
  `last_login` varchar(50) NOT NULL DEFAULT '1980-01-01 00:00:00',
  `last_access` varchar(50) NOT NULL DEFAULT '1980-01-01 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=200 DEFAULT CHARSET=utf8 CHECKSUM=1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `mails`
--

DROP TABLE IF EXISTS `mails`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mails` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `from` text,
  `subject` text,
  `body` text,
  `entry_id` varchar(255) DEFAULT NULL,
  `rt` varchar(200) DEFAULT NULL,
  `mail_address` varchar(200) DEFAULT NULL,
  `came_from` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1 mail, 2 manual',
  `deleted` tinyint(3) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9635 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `nomail_attachements`
--

DROP TABLE IF EXISTS `nomail_attachements`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `nomail_attachements` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `book_id` int(10) unsigned NOT NULL,
  `entry_id` varchar(200) NOT NULL DEFAULT '0',
  `file_name` varchar(200) NOT NULL,
  `rem` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_nomail_attachements_1` (`book_id`),
  CONSTRAINT `FK_nomail_attachements_1` FOREIGN KEY (`book_id`) REFERENCES `book` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `place`
--

DROP TABLE IF EXISTS `place`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `place` (
  `id` int(5) NOT NULL AUTO_INCREMENT,
  `i_place` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=289 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `registry`
--

DROP TABLE IF EXISTS `registry`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `registry` (
  `lock_status` int(11) NOT NULL DEFAULT '0',
  `lock_date` text NOT NULL,
  `lock_time` text NOT NULL,
  `lock_timestamp` double NOT NULL DEFAULT '0',
  `mail_id` bigint(20) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `thematologio`
--

DROP TABLE IF EXISTS `thematologio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `thematologio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fakelos` varchar(45) DEFAULT NULL,
  `perigrafi` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=233 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-03 11:17:23
