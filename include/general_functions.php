<?php

require_once "constants.inc";

    function header_function($mode=0)
    {
        $max_val=0;
        $dep_id=0;
        if(isset($_SESSION['max_val']))
            $max_val=$_SESSION['max_val'];
        if(isset($_SESSION['dep_id']))
            $dep_id=$_SESSION['dep_id'];
        echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
        echo "<html xmlns=\"http://www.w3.org/1999/xhtml\">";
        echo "<head>";
        echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />";
        echo "<title>".YPHRESIA." e-protocol</title>";

        echo "<link rel=\"stylesheet\" type=\"text/css\" href=\"./css/styles.css\" />";
        echo "<script type=\"text/javascript\" src=\"./js/jquery-1.4.js\"></script>";
        if($mode==0)
            echo "<script type=\"text/javascript\" src=\"./js/module.js\"></script>";
        else if($mode==1)
            echo "<script type=\"text/javascript\" src=\"./js/module_imports.js\"></script>";
            
		echo "<script type=\"text/javascript\" src=\"./js/module_new.js\"></script>"; 
		
        echo "<link rel=\"stylesheet\" type=\"text/css\"  href=\"./js/themes/purple-theme/jquery.ui.all.css\">";
        //<script src="../../jquery-1.7.1.js"></script>
        //<!--// <script src="../../external/jquery.bgiframe-2.1.2.js"></script>  //-->
        echo "<script src=\"./js/ui/jquery.ui.core.js\"></script>";
        echo "<script src=\"./js/ui/jquery.ui.widget.js\"></script>";
        echo "<script src=\"./js/ui/jquery.ui.mouse.js\"></script>";
        echo "<script src=\"./js/ui/jquery.ui.button.js\"></script>";
        echo "<script src=\"./js/ui/jquery.ui.draggable.js\"></script>";
        echo "<script src=\"./js/ui/jquery.ui.position.js\"></script>";
        echo "<script src=\"./js/ui/jquery.ui.resizable.js\"></script>";
        echo "<script src=\"./js/ui/jquery.ui.dialog.js\"></script>";
        echo "<script src=\"./js/ui/jquery.effects.core.js\"></script>";
        echo "<script src=\"./js/ui/jquery.ui.datepicker.js\"></script>";
        echo "<script src=\"./js/ui/i18n/jquery.ui.datepicker-el.js\"></script>";
        
        // vassilis autocomplete
        //echo "<script type=\"text/javascript\" src=\"./js/jquery.min.js\"></script>";
        echo "<script type=\"text/javascript\" src=\"./js/script.js\"></script>";
        // vassilis autocomplete

        //echo "<link rel=\"stylesheet\" href=\"./css/demos.css">
        echo "</head>";

        echo "<body>";

        echo "<div id=\"bodywatermark\">";
        echo "<div id=\"mcontainer\">";

        echo "<div id=\"header\"><div id=\"headerwatermark\">";

        echo "<div id=\"headerpad\">";
            echo "<span class=\"slogan\">e-protocol v2.0</span>";
            echo "<span class=\"user\">Χρήστης: ".$_SESSION['dep_name']."<br></span>";
            echo "<span class=\"tit\">".YPHRESIA."</span><br />";
            echo "<span class=\"subtit\">Ηλεκτρονικό Πρωτόκολλο 'ΑΘΗΝΑ'</span>";
            if(HASMAIL){
             echo "<span class=\"subtitinfo\" id=\"new_mail_info\">";
            echo "0 " . date("d/m/Y H:i:s") . "<a href=\"./\"><img src=\"./images/recycle.png\" alt=\"refresh\" /></a>";}
            echo "<form name=\"frmnewmail\" id=\"frm_id\" method=\"post\" action=\"\">";
            echo "<input type=\"hidden\" name=\"newmails\" value=\"" . $max_val . "\" />";
            echo "<input type=\"hidden\" name=\"dep_id\" value=\"" . $dep_id . "\" />";
            echo "</form></span>";
            //echo "<span class=\"slogan\">e-protocol</span>";
        echo "</div>";

        echo "</div>" ; //<!--End of Header Watermark -->
        echo "</div>"; //<!--End of Header -->

        return;
    }
    
    function menu_hor_function($userlevel='')
    {

        echo "<div class=\"mmenu\">";
         echo "<ul>";

                echo "<li><span>-</span></li>";

				if($userlevel > 1) {
					echo "<li><a href=\"./departments.php\">Τμήματα</a></li>";
				}
				
                
                if($userlevel > 1)
                    if(HASMAIL==1){ 
                        echo "<li><a href=\"./index.php\">e-mails</a></li>";
                        if($userlevel==66){
                            echo "<li><a href=\"./diegrammena.php\">Διεγραμμένα_e-mails</a></li>";
                            echo "<li><a href=\"./multiple.php\">Πολλά πρωτόκολλα</a></li>";
                        }
                        
                    }
                echo "<li><a href=\"./outports.php?st=0\">Εισερχόμενα</a></li>";
                echo "<li><a href=\"./outports.php?st=1\">Εξερχόμενα</a></li>";
                //echo "<li><span style=\"font-size:8pt;font-weight:bold;color:#ffffff;margin-top:0px;\">" . $_SESSION['dep_name'] . "<a  href=\"./eprotocol_guide.pdf\" target=\"_blank\"><img src=\"./images/help.png\"  alt=\"help\" /></a></span></li>";
                //echo "<li><a  href=\"./eprotocol_guide.pdf\" target=\"_blank\" style=\"color:#ffffff;\">" . $_SESSION['dep_name'] . " <img src=\"./images/help.png\"  alt=\"help\" /></a></li>";
                echo "<li><a href=\"./ypografes_export.php\" target=\"_blank\">Υπογραφές</a></li>";
                
                echo "<li><a href=\"./logout.php\">Έξοδος</a></li>";
        echo "</ul>";

        echo "<div class=\"clear\"></div>";
        echo "</div>";
        
        return;
    }
    
    
    function left_side_function($dept_id=0)
    {
        //return;
        echo "<div id=\"leftcolumn\">";

        echo "<div class=\"leftcolumnpad\">";
        echo "<img class=\"paperclip\" src=\"./images/paperclip.png\" alt=\"paperclip\">";
            echo "<h2>Menu</h2>";
            echo "<ul>";
            switch ($dept_id)
            {
                case 0:
                    //echo "<li><button id=\"create-user\">Create new user</button></li>";
                break;
                case 1:
                    echo "<li><a href=\"./eprotocol_guide.pdf\" target=\"_blank\">Βοήθεια <img src=\"./images/help.png\"  alt=\"help\" /></a></li>";
                    //echo "<li><a href=\"./imports.php\">Εισερχόμενα</a></li>";
                    //echo "<li><a href=\"./outports.php\">Εξερχόμενα</a></li>";
                break;
                
            }
            echo "</ul>";

        echo "</div>"; //<!-- end of padding -->

        echo "</div>";
        
        return;

    }
    
    function footer_function()
    {

        echo "<div id=\"footer\">";
        echo "<div id=\"footerpad\">";
       // echo "<p>";
         //   echo "<a href=\"./index.php\">Home Page</a> -
         //   <a href=\"#\">Contact Us</a> -
        //    <a href=\"#\">Site Map</a> -
        //    <a href=\"http://www.freecssmenus.co.uk/web_template_builder.php\" target=\"_blank\">Free Website Builder</a>";
       // echo "</p>";
            //echo "<p>This Website is Tableless &amp; Xhtml/CSS Strict.</p>";
        echo "</div>";

    echo "</div>"; //<!--End of footer -->


    echo "</div>"; //<!--End of Main Container -->

    echo "</div>"; // <!-- end of bodywatermark -->

    echo "</body>";
    echo "</html>";
    return;
}
function selectproter($controlName, $selectedVal, $extra_arg = "") {
   $query = "SELECT id, perigrafi FROM proteraiothta  WHERE 1 = 1";
    $query .= " ORDER BY id";
    $result = mysql_query($query);
    $mes = "<select name=\"$controlName\" $extra_arg>\n";
   // $mes .= "<option value=\"\">(Επιλέξτε Προτεραιότητα)</option>\n";
    for ($i = 0; $i < mysql_num_rows($result); $i++) {
        $row = mysql_fetch_array($result);
        $mes.= "<option value=\"" . $row["id"] . "\" " . (($row["id"] == $selectedVal) ? "selected=\"1\"" : "") . ">" . $row["perigrafi"] . "</option>\n";
    }
    $mes.= "</select>\n";
    
    return $mes;
}
function footer_small_function()
{
    echo "</div>"; //<!--End of Main Container -->

    echo "</div>"; // <!-- end of bodywatermark -->

    echo "</body>";
    echo "</html>";
    return;
}
    
    
?>
