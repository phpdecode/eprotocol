
$(document).ready(function(){

    $("#frmimport_id, frmsrchimp_id").submit(function(event) {

    /* stop form from submitting normally */
    event.preventDefault();
    });
    $("#srch_io_date_id, #srch_io_date_to_id,#io_auth_date_id").live("mouseover",
    function(){
        $(this).datepicker();
        $.datepicker.regional[ "el" ];
    });

    $("a.imports").live("click",function(event){

    event.preventDefault();
    var y=$(window).scrollTop();
    $('#cur_page_id').val(y);
    var str_id= this.id;
    var tbl=str_id.split("_");
    $("#book_id_id").val(tbl[1]);
    
    $.post("./include/imports_task.php",
        $("#frmimport_id").serialize(),
        function(response){
        $("#frmimport_id").html(response);
                   // alert('ok');

        }
        );

    $("#list_id").css("display","none");
    $("#form_id").css("display","block");

    
  });
  
  $("#sbmtsrch_id").live("mouseover",function(){
    $(this).removeClass("ui-state-active").addClass("ui-state-hover");
  });
  
   $("#sbmtsrch_id").live("mouseout",function(){
    $(this).removeClass("ui-state-hover").addClass("ui-state-active");
  });
  $("#sbmtsrch_id").live("mousedown",function(ev){
    $(this).removeClass("ui-state-hover").addClass("ui-state-highlight");
  });
  
  $("#sbmtsrch_id").live("mouseup",function(ev){
    $(this).removeClass("ui-state-highlight").addClass("ui-state-hover");
  });
  
  
  
  $("input,textarea").live("mouseover",function(){
        $(this).css("border", "1px solid #000000");
        $(this).closest("div").css("color","#462D52");
    });

    $("input, textarea").live("mouseout",function(){
        $(this).closest("div").css("color","#000000");
        $(this).css("border", "1px solid #a0a0a0");
    });


     $("a.hid").live("click",function(){
    var id= this.id;
     var obj ="span#b_" + id ;
     //alert(obj);
    $(obj).toggle('slow');

    return false;
  });
  
  $("input#btnrtn").live("click",function(){

    $("#chfl_id").val(5);
    $.post("./include/outports_list.php",
        $("#frmsrchimp_id").serialize(),
        function(response){
        $("#list_id").html(response);
                  // alert($("#frmsrchimp_id").serialize());

        }
        );
    
    
    
    $("#list_id").css("display","block");
    $("#form_id").css("display","none");
    $(window).scrollTop($('#cur_page_id').val());

  });
     /************************************************/
  $("a#newimport").live("click",function(event){
    event.preventDefault();

    $("#book_id_id").val(0);
    $("#chfl_id").val(0);

    $.post("./include/imports_task.php",
        $("#frmimport_id").serialize(),
        function(response){
        $("#frmimport_id").html(response);


        }
        );

    $("#list_id").css("display","none");
    $("#form_id").css("display","block");


  });
   /***************************************/
  $("a.newoutfromin").live("click",function(event){
    event.preventDefault();

    var id=parseInt(this.id,10);

    $("#book_id_id").val(id);
    $("#chfl_id").val(0);


    $.post("./include/imports_task.php",
        $("#frmimport_id").serialize(),
        function(response){
        $("#frmimport_id").html(response);


        }
        );

    $("#list_id").css("display","none");
    $("#form_id").css("display","block");


  });

  
  

  $("input#addup").live("click",function(event){
    event.preventDefault();
    //var uploads=$("div#uploads");
    //$("<p><label for=\"uploaded_file[]\" >Αρχείο</label><input name=\"uploaded_file[]\" type=\"file\" /></p>").appendTo(uploads);
    //window.open('./index.php','Continue_to_Application','width=600,height=200');
    $("#fieldset_id").css("display","none");
    $("#uploads").css("display","inline-block");
    

    return false;

  });
  
  $("a.aupload").live("click",function(event){

    event.preventDefault();
    var str_id= this.id;
    var tbl=str_id.split("_");
    $("#book_id_id").val(tbl[1]);
    $("#chfl_id").val(5);

    $.post("./include/imports_task.php",
        $("#frmimport_id").serialize(),
        function(response){
        $("#frmimport_id").html(response);
                   // alert('ok');

        }
        );
    
    //alert('ok');
    $("#fieldset_id").css("display","inline");
    $("#uploads").css("display","none");
    //alert('ok2');


  });
  
  $("#accept_id").live("click",function(event){


    $.post("./include/imports_task.php",
        $("#frmimport_id").serialize(),
        function(response){
        //var tblresponse = response.split("<separator_tag/>");
        //if(tblresponse.length==2)
        //{
        //    $("#frmimport_id").html(tblresponse[0]);
        //    $('#tbl_list_id tr:first').after(tblresponse[1]);

        //}
        //else
            $("#frmimport_id").html(response);

                   //alert($("#frmimport_id").serialize());

        }
        );

  });
  
  
   $("#sbmtsrch_id").live("click",function(event){
   //alert('ok');
   $('#page_id').val(0);
    $.post("./include/outports_list.php",
        $("#frmsrchimp_id").serialize(),
        function(response){
        $("#list_id").html(response);
                  // alert($("#frmsrchimp_id").serialize());

        }
        );
  });
  
  
  $("#page_id").live("change",function(event){

    //$('#cur_page_id').val($(this).val());
    $.post("./include/outports_list.php",
        $("#frmsrchimp_id").serialize(),
        function(response){
        $("#list_id").html(response);
                  // alert($("#frmsrchimp_id").serialize());

        }
        );

  });
  
  
 refresh();
}); // ready


function accept_button_click (id)
{
	var arg = $("#frmimport_id").serialize() + "&import_id=" + id;
	
    $.post ("./include/imports_task.php",
        arg,
        function(response){
            $("#frmimport_id").html(response);
        }
    );
}

function change_page (page)
{
	if (page)
	{
		var max_pages = document.getElementById ("page_id").length - 1;
		var set_page = parseInt (document.getElementById ("page_id").value) + page;
		if (set_page < 0) set_page = 0;
		else if (set_page > max_pages) set_page = max_pages;	
		document.getElementById ("page_id").value = set_page;
	}
	$.post("./include/outports_list.php", $("#frmsrchimp_id").serialize(),
		function(response) {
			$("#list_id").html(response);
		}
	);
}

function refresh()
    {
        //alert("ok");
        $.post("./include/refresh.php",
            $("#frm_id").serialize(),
            function(response){
            $("#new_mail_info").html(response);
               //alert(response);
            }
            );
             //alert($("#frm_id").serialize());
             //alert(document.getElementById('new_mail_info').innerHTML);
            setTimeout("refresh()",10000);


    }

