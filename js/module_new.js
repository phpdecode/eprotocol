/* New functions */

function receive_emails_from_server2 ()
{
	$.ajaxSetup ({
		cache: false 
	});
	var xml = 'NO';
	var elem1, elem2, elem3, elem4;
	elem1 = document.getElementById ('modal_frame');
	if (elem1) elem1.style.display = 'block';
	elem2 = document.getElementById ('modal_message');
	if (elem2) elem2.style.display = 'block';
	elem3 = document.getElementById ('modal_info');
	if (elem3) elem3.innerHTML = 'Παρακαλώ περιμένετε...';
	elem4 = document.getElementById ('modal_button');
	if (elem4) elem4.style.display = 'none';
	$.ajax({
		cache: false,
		type: "GET",
		url: "./index.php?act=getemails",
		dataType: "text",
		async: false,
		error : function (XMLHttpRequest, textStatus, errorThrown) {
              alert ('Something went wrong: ' + XMLHttpRequest + ' , ' + errorThrown);
        },
		success : function(data) {
				var nomsg = parseInt (data); xml = data;
                var msg = ''
			    if (nomsg == -100) msg = 'Διαδικασία σε εξέλιξη από άλλο χρήστη.<br />Δοκιμάστε ξανά αργότερα';
				else if (nomsg < 0) msg = 'Αγνωστο σφάλμα - Κωδ: ' + nomsg;
				else msg = 'Νέα μηνύματα: ' + nomsg;
                if (elem3) elem3.innerHTML = msg
                if (elem4) elem4.style.display = '';
				// if (elem1) elem1.style.display = 'none';
				// if (elem2) elem2.style.display = 'none';
        }
	});
}

function edit_department (id, name, desc, username, password, level, hidden)
{
	var elem1, elem2;

	elem1 = document.getElementById ('modal_dpt_frame');
	if (elem1) elem1.style.display = 'block';
	elem2 = document.getElementById ('modal_dpt_message');
	if (elem2) elem2.style.display = 'block';
	
	document.getElementById ('dep_edit_name').value = name;
	document.getElementById ('dep_edit_desc').value = desc;
	document.getElementById ('dep_edit_username').value = username;
	document.getElementById ('dep_edit_password').value = password;
	document.getElementById ('dep_edit_level').value = level;
	document.getElementById ('dep_edit_hidden').value = hidden;
	document.getElementById ('dep_edit_original_username').value = username;
	document.getElementById ('dep_edit_id').value = id;
	
}
