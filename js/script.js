
// autocomplet : this function will be executed every time we change the text
function autocomplet() {
   
	var keyword = $('#fakelos_id').val();
	$.ajax({
		url: 'refresh.php?num=1',
                minLength:2,
		type: 'POST',
		data: {keyword:keyword},
		success:function(data){
			$('#fakelos_list_id').show();
			$('#fakelos_list_id').html(data);
		}
	});
}

function autocomplet2() {
	var keyword = $('#io_auth_id').val();
	$.ajax({
		url: 'refresh.php?num=2',
                minLength:2,
		type: 'POST',
		data: {keyword:keyword},
		success:function(data){
			$('#io_auth_list_id').show();
			$('#io_auth_list_id').html(data);
		}
	});
}

function autocomplet3() {
	var keyword = $('#i_place_id').val();
	$.ajax({
		url: 'refresh.php?num=3',
                minLength:2,
		type: 'POST',
		data: {keyword:keyword},
		success:function(data){
			$('#i_place_list_id').show();
			$('#i_place_list_id').html(data);
		}
	});
}
// set_item : this function will be executed when we select an item
function set_item(item) {
	// change input value
	$('#fakelos_id').val(item);
	// hide proposition list
	$('#fakelos_list_id').hide();
}

function set_item2(item) {
	// change input value
	$('#io_auth_id').val(item);
	// hide proposition list
	$('#io_auth_list_id').hide();
}

function set_item3(item) {
	// change input value
	$('#i_place_id').val(item);
	// hide proposition list
	$('#i_place_list_id').hide();
}