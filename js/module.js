
$(document).ready(function(){



    $("#frmnewmail_id").submit(function(event) {

    /* stop form from submitting normally */
    event.preventDefault();
    });
    
    $("#io_date_id, #io_auth_date_id").live("mouseover",
    function(){
        $(this).datepicker();
        $.datepicker.regional[ "el" ];
    });

    
    
    
    $(function() {
        // a workaround for a flaw in the demo system (http://dev.jqueryui.com/ticket/4375), ignore!
        $( "#dialog:ui-dialog" ).dialog( "destroy" );

        var name = $( "#name" ),
            email = $( "#email" ),
            password = $( "#password" ),
            allFields = $( [] ).add( name ).add( email ).add( password ),
            tips = $( ".validateTips" );

        function updateTips( t ) {
            tips
                .text( t )
                .addClass( "ui-state-highlight" );
            setTimeout(function() {
                tips.removeClass( "ui-state-highlight", 1500 );
            }, 500 );
        }

        function checkLength( o, n, min, max ) {
            if ( o.val().length > max || o.val().length < min ) {
                o.addClass( "ui-state-error" );
                updateTips( "Length of " + n + " must be between " +
                    min + " and " + max + "." );
                return false;
            } else {
                return true;
            }
        }

        function checkRegexp( o, regexp, n ) {
            if ( !( regexp.test( o.val() ) ) ) {
                o.addClass( "ui-state-error" );
                updateTips( n );
                return false;
            } else {
                return true;
            }
        }

        $( "#dialog-form" ).dialog({
            autoOpen: false,
            height:650,
            width: 550,
            modal: true,
            buttons: {
                "Εισαγωγή": function() {
                
                //event.preventDefault();
                var x='#d_' + $("#idid").val() ;
                $("#status_id").val(0) ;
                $("#check_flag_id").val(1) ;    //insert
                $("p.validateTips").html("Ενημέρωση Εισερχ");


                $.post("./include/aaa.php",
                $("#frm_record_id").serialize(),
                function(response){
                     if(parseInt(response,10)>0)
                     {
                        
                        $(x).html('Εισερχόμενο με Α.Π: ' + response);
                        $("#dialog-form").dialog("close");

                        $(x).css("background-color","#f0f0f0");//queue(function() {
                            //$(x).
                        //});


                        
                     }
                     else
                        $("#frm_record_id").html(response);
                    //alert('ok');
                }
                );


                        //$( this ).dialog("close");
                },
                Άκυρο: function() {
                    $( this ).dialog("close");
                }
            },
            close: function() {
                allFields.val( "" ).removeClass( "ui-state-error" );
            }
        });


        $( "a.create-import" ).click(function(event) {
                event.preventDefault();
                $("#idid").val(this.id) ;
                $("#status_id").val(0) ;
                $("#check_flag_id").val(0) ;    //insert
                $("p.validateTips").html("Εισαγωγή νέου Εισερχομένου");


                $.post("./include/aaa.php",
                $("#frm_record_id").serialize(),
                function(response){
                    $("#frm_record_id").html(response);
                    //alert('ok');

                }
                );

                $( "#dialog-form" ).dialog( "open" );

                
                //$("#name").focus();
                //return false;

            });
            $( "a.print_mail" ).click(function(event) {
                event.preventDefault();
                
              var id=parseInt(this.id,10);
              var x='#d_'+ id;
              
              $("#idid").val(id);
              
              $.post("./include/print_mail.php",
                $("#frm_record_id").serialize(),
                function(response){
                    if(response > 0)
                    {
                        //alert('ok');
                        $(x).remove();
                    }
                    else
                    {
                        alert(response);
                    }

                }
                );
              
            });
            $( "a.return_mail" ).click(function(event) {
                event.preventDefault();
                
              var id=parseInt(this.id,10);
              var x='#d_'+ id;
              
              $("#idid").val(id);
              
              $.post("./include/return_mail.php",
                $("#frm_record_id").serialize(),
                function(response){
                    if(response > 0)
                    {
                        //alert('ok');
                        $(x).remove();
                    }
                    else
                    {
                        alert(response);
                    }

                }
                );
              
            });
            $( "a.delete_mail" ).click(function(event) {
                event.preventDefault();
                
              var id=parseInt(this.id,10);
              var x='#d_'+ id;
              
              $("#idid").val(id);
              
              $.post("./include/delete_mail.php",
                $("#frm_record_id").serialize(),
                function(response){
                    if(response > 0)
                    {
                        //alert('ok');
                        $(x).remove();
                    }
                    else
                    {
                        alert(response);
                    }

                }
                );
              
            });

        $("input,textarea").live("mouseover",function(){
        $(this).css("border", "1px solid #000000");
        $(this).closest("div").css("color","#462D52");
    });

    $("input, textarea").live("mouseout",function(){
        $(this).closest("div").css("color","#000000");
        $(this).css("border", "1px solid #a0a0a0");
    });
    });

    
    

    
    refresh();
      /*****
     $("a.hid").mouseover(function(){
    var id= this.id;
     var obj ="span#b_" + id ;
     //alert(obj);
    $(obj).show('slow');

    return false;
  });
  *******/
  $("a.hid").click(function(){
    var id= this.id;
     var obj ="span#b_" + id ;
     //alert(obj);
    $(obj).toggle('slow');

    return false;
  });
  

    
    
    
    
}); // ready

function refresh()
    {
        //alert("ok");
        $.post("./include/refresh.php",
            $("#frm_id").serialize(),
            function(response){
            $("#new_mail_info").html(response);

            }
            );
            // alert($("#frm_id").serialize());
             //alert(document.getElementById('new_mail_info').innerHTML);
            setTimeout("refresh()",180000);


    }


