<?php
    date_default_timezone_set('Europe/Athens');

    DEFINE("DB_USER", "");
    DEFINE("DB_PASSWORD", "");
    DEFINE("DB_HOST", "");
    DEFINE("DB_NAME", "");
    DEFINE("MAIL_PP",50); //dev
    DEFINE("APP_DIR", "");
    DEFINE("YPHRESIA","");
    DEFINE("KODIKOS_DDE",);//3-digit
    DEFINE("HASMAIL",1);// 0 no -- 1 yes
    DEFINE("APP_URL", "http://" . $_SERVER['SERVER_NAME'] . APP_DIR);
	
    @DEFINE ("MAIL_HOST", "{mail.lar.sch.gr:143/imap/notls}INBOX");
    
    @DEFINE ("MAIL_LOGIN", "");
    @DEFINE ("MAIL_PASSWORD", ''); 
    
    DEFINE ("ATTACHMENT_FOLDER", "");
        
    DEFINE ("BODY_MESSAGE_SIZE", -1);
    DEFINE ("FILENAME_ENCODING", "utf-8"); // LINUX

	// Method for mail transfering:
	// 1: using original email account (reads ALL emails and gets the most recent ones : slower but safer)
	// 2: using secondary email account (reads and gets UNSEEN emails only : faster, safe only if a secondary email account is used)
    DEFINE ("MAIL_TRANSFER_METHOD", 1);
	
	//error_reporting (0);
	
?>
