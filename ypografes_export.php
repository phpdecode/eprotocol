<?php
session_start();
require_once('./lib/tcpdf/tcpdf.php');
require_once("./include/connect_functions.php");
   
$day = $_POST['day'];
$month = $_POST['month']; 
$year = $_POST['year'];
$tmhma = $_POST['tmhma'];

if(ISSET($month) && ISSET($day) && ISSET($year) && ISSET($tmhma)){

set_time_limit(0);
connect_db();
mysql_query('set character set utf8');

        $pdf = new TCPDF('L', 'mm', 'A4', true, 'UTF-8', false);
        $pdf->setFontSubsetting(false);
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetMargins(5, 1, 5);

        $calibri_font = $pdf->addTTFfont('./fonts/calibri.ttf', 'TrueTypeUnicode', '', 32);
        $calibri_font1 = $pdf->addTTFfont('./fonts/calibrib.ttf', 'TrueTypeUnicode', '', 32);
        $calibri_font2 = $pdf->addTTFfont('./fonts/calibrii.ttf', 'TrueTypeUnicode', '', 32);
        $calibri_font3 = $pdf->addTTFfont('./fonts/calibriz.ttf', 'TrueTypeUnicode', '', 32);

       $pdf->AddPage('L', 'A4');
        $pdf->SetAutoPageBreak(true, 1.5);
        $line = 2;
        $pdf->SetFont($calibri_font, '', 10);

        $pdf->Cell(LINE_WIDTH, 4, "", 0, 1, 'L'); $pdf->Line(5);
        $pdf->Cell(10, 4, "Α.Π.", 1, 0, 'C');
        $pdf->Cell(17, 4, "Ημ/νια", 1, 0, 'C');
        $pdf->Cell(20, 4, "Αρ. Εισ.", 1, 0, 'C');
        $pdf->Cell(20, 4, "Τοπ. Εκδ.", 1, 0, 'C');
        $pdf->Cell(30, 4, "Αρχή. Εκδ.", 1, 0, 'C');
        if(KODIKOS_DDE==284){
            $pdf->Cell(100, 4, "Περ. Εισ.", 1, 0, 'C');
            $pdf->Cell(40, 4, "Πληροφορίες", 1, 0, 'C');
            $pdf->Cell(40, 4, "Τμήμα", 1, 0, 'C');
        }
        else {
            $pdf->Cell(130, 4, "Περ. Εισ.", 1, 0, 'C');
            $pdf->Cell(50, 4, "Τμήμα", 1, 0, 'C');
        }
        $pdf->Cell(10, 4, "Yπογ", 1, 1, 'C');
        
        $print_date = $day."-".$month."-".$year;
        $from_date=$year."-".$month."-".$day; 
        $to_date=$from_date;
      
        if($tmhma==1)
            $dep = " and bd.department_id in (1)";
        else if($tmhma==2)
            $dep = " and bd.department_id in (4) ";
        
        
$query = "select distinct b.* ,ap.io_auth as AUTH,pl.i_place AS PLACE from book b "
        . "inner join bookdep bd on bd.book_id=b.id "
        . " left join apopros ap on ap.id=b.io_auth "
        . " left join place pl on pl.id=b.i_place "
        . "where b.status=0 and b.io_date between '$from_date' and '$to_date' ";
if ($tmhma==1 || $tmhma==2)
    $query .= $dep;
$query .= " order by  b.pn asc, b.io_date asc";
//echo $query;
$res = mysql_query($query);
$num_res = mysql_num_rows($res);
if ($num_res > 0) {
    $pdf->SetFont($calibri_font, '', 8);
    for($i=0; $i<$num_res; $i++){ $j=$i+1;
        $row = mysql_fetch_array($res);
        $pdf->MultiCell(10, 30, $row['pn'],'LRB', 'C', 0, 0, '', '', true);
        $pdf->MultiCell(17, 30, $print_date,'LRB', 'C', 0, 0, '', '', true);;
        $pdf->MultiCell(20, 30, $row['i_num'], 'LRB', 'C', 0, 0, '', '', true);
        if(is_numeric($row['i_place']))
           $pdf->MultiCell(20, 30, $row['PLACE'],'LRB', 'C', 0, 0, '', '', true);
        else
            $pdf->MultiCell(20, 30, $row['i_place'],'LRB', 'C', 0, 0, '', '', true);
        if(is_numeric($row['io_auth']))
            $pdf->MultiCell(30, 30, $row['AUTH'], 'LRB', 'C', 0, 0, '', '', true);
        else
            $pdf->MultiCell(30, 30, $row['io_auth'], 'LRB', 'C', 0, 0, '', '', true);
        if(KODIKOS_DDE==284){
            $pdf->MultiCell(100, 30, $row['summary'],'LRB', 'C', 0, 0, '', '', true);
            $pdf->MultiCell(40, 30, $row['rem'],'LRB', 'C', 0, 0, '', '', true);
        }
        else
            $pdf->MultiCell(130, 30, $row['summary'],'LRB', 'C', 0, 0, '', '', true);
        $qry = "select b.book_id, b.department_id, d.name from bookdep b inner join departments d on b.department_id=d.id where b.book_id=" . $row['id'];
	@mysql_query ('set character set utf8 ');
	$resdep = mysql_query ($qry);
	$num_dep = mysql_num_rows($resdep); $depnames="";
        if ($num_dep > 0)
        {
                for($κ = 0 ; $κ < $num_dep ; $κ++)
                {
                        $rowdep = mysql_fetch_array($resdep);
                        if($num_dep<=4)
                            $depnames .=$rowdep['name']."\n\n";
                        else 
                            $depnames .=$rowdep['name']."\n";
                        
                    
                }
         if(KODIKOS_DDE=="284"){
             $pdf->MultiCell(40, 30, $depnames,'LRB', 'C', 0, 0, '', '', true);
         }
         else
            $pdf->MultiCell(50, 30, $depnames,'LRB', 'C', 0, 0, '', '', true);
        }
        else {
            $pdf->MultiCell(50, 30, "",'LRB', 'C', 0, 0, '', '', true);
        }
        $pdf->MultiCell(10, 30, "",'LRB', 'C', 0, 1, '', '', true);
        if($j % 6 == 0){
                $pdf->AddPage('L', 'A4');
                $pdf->SetFont($calibri_font, '', 10);

                $pdf->Cell(LINE_WIDTH, 4, "", 0, 1, 'L'); $pdf->Line(5);
                $pdf->Cell(10, 4, "Α.Π.", 1, 0, 'C');
                $pdf->Cell(17, 4, "Ημ/νια", 1, 0, 'C');
                $pdf->Cell(20, 4, "Αρ. Εισ.", 1, 0, 'C');
                $pdf->Cell(20, 4, "Τοπ. Εκδ.", 1, 0, 'C');
                $pdf->Cell(30, 4, "Αρχή. Εκδ.", 1, 0, 'C');
                if(KODIKOS_DDE==284){
                    $pdf->Cell(100, 4, "Περ. Εισ.", 1, 0, 'C');
                    $pdf->Cell(40, 4, "Πληροφορίες", 1, 0, 'C');
                    $pdf->Cell(40, 4, "Τμήμα", 1, 0, 'C');
                }
                else {
                    $pdf->Cell(130, 4, "Περ. Εισ.", 1, 0, 'C');
                    $pdf->Cell(50, 4, "Τμήμα", 1, 0, 'C');
                }
                $pdf->Cell(10, 4, "Yπογ", 1, 1, 'C');
                $pdf->SetFont($calibri_font, '', 8);
        }

    }}                     
       ob_clean();
        $pdf->Output();
}    
 else {   
   echo "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">";
        echo "<html xmlns=\"http://www.w3.org/1999/xhtml\">";
        echo "<head>";
        echo "<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\" />";
        echo "<title>ΔΔΕ Λάρισας e-protocol</title>";
    echo "</head><body>";
   echo "<form name=\"frm1\" action=\"\" method=\"post\">";
   echo "<table><tr><td>";
   echo "Ημέρα:</td><td><input type=\"text\" name=\"day\"/></td></tr>";
   echo "<tr><td>Μήνας: </td><td><input type=\"text\" name=\"month\"/></td></tr>";      
   echo "<tr><td>Έτος: </td><td><input type=\"text\" name=\"year\"/></td></tr>";
   echo "<tr><td>Τμήμα (0 Όλα, 1 Διοικ., 2 Εκπ.): </td><td><input type=\"text\" name=\"tmhma\"/></td></tr>";
   echo "<tr><td>&nbsp;</td><td><input type=\"submit\" value=\"Εξαγωγή\"></tr></table>";
   echo "</form>";
   echo "</body></html>";
    
 }
?>
